input_file =open("input_file.txt", "r")
myfile=""
for line in input_file:
   myfile+=line
file_data=(myfile.split("\n"))

width,height=file_data[0].split(" ")
width=int(width)
height=int(height)


user_x,user_y,direction=file_data[1].split(" ")
user_x1=int(user_x)
user_y1=int(user_y)
direction=direction.upper()


x1,y1,dir1=(file_data[2].split(" "))
x1=int(x1)
y1=int(y1)
dir1=dir1.lower()

x2,y2,dir2=file_data[3].split(" ")
x2=int(x2)
y2=int(y2)
dir2=dir2.lower()

x3,y3,dir3=file_data[4].split(" ")
x3=int(x3)
y3=int(y3)
dir3=dir3.lower()

x4,y4,dir4=file_data[5].split(" ")
x4=int(x4)
y4=int(y4)
dir4=dir4.lower()


x5,y5,dir5=file_data[6].split(" ")
x5=int(x5)
y5=int(y5)
dir5=dir5.lower()



mirror_list=[[x1,y1,dir1],[x2,y2,dir2],[x3,y3,dir3],[x4,y4,dir4],[x5,y5,dir5]]

player=[user_x1, user_y1, direction]

flag=True
count=0

while(flag):
    if(player[2]=="S"):
        if (player[0]<width and player[1]<height and player[0]>=0 and player[1]>=0):
            player[1]=(player[1]-1) 
            print("move_player", player)          
            count=count+1 
            for i in mirror_list:
                if(player[0] == i[0] and player[1] == i[1]):   
                    if(i[2]=="b"):      
                        print("moving to east")
                        player[2]="E"
                        continue
                    else:
                        print("moving to west")
                        player[2]="W"
                        continue
                else:
                    continue
        else:         
            print("hitting")
            flag=False
    elif(player[2]=="E"):
        if (player[0]<width and player[1]<height and player[0]>=0 and player[1]>=0):
            
            player[0]=(player[0]+1)  
            print("move_player", player)
            count=count+1   
            for i in mirror_list:
                if(player[0] == i[0] and player[1] == i[1]):
                    if(i[2]=="b"):
                        print("moving to south")
                        player[2]="S"
                        continue
                    else:
                        print("moving to north")
                        player[2]="N"
                        continue
                else:
                    continue
        else:
            print("hitting")
            flag=False
    
    elif(player[2]=="W"):
        if (player[0]<width and player[1]<height and player[0]>=0 and player[1]>=0):
            
            player[0]=(player[0]-1) 
            print("move_player", player)
            count=count+1 
            for i in mirror_list:
                if(player[0] == i[0] and player[1] == i[1]):
                    if(i[2]=="b"):
                        print("moving to north")
                        player[2]="N"
                        continue
                    else:
                        print("moving to south")
                        player[2]="S"
                        continue
                else:
                    continue
        else:
            print("hitting")
            flag=False
    
    elif(player[2]=="N"):
        if (player[0]<width and player[1]<height and player[0]>=0 and player[1]>=0):
            
            player[1]=(player[1]+1)
            print("move_player", player)
            count=count+1 

            for i in mirror_list:
                if(player[0] == i[0] and player[1] == i[1]):
                    if(i[2]=="b"):
                        print("moving to west")
                        player[2]="W"
                        continue
                    else:
                        print("moving to east")
                        player[2]="E"
                        continue

                else:
                    continue
        else:
            print("hitting")
            flag=False
print(count-1)